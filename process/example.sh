#!/bin/bash

# Path to the GCD file
gcd=/data/sim/IceCube/2010/filtered/level2a/GeoCalibDetectorStatus_IC79.55380_L2a.i3.gz
# Path to the data file(s). This can be just one data file or a globbed expression.
datafiles=/data/sim/IceCube/2010/filtered/level2a/CORSIKA-in-ice/8316/00000-00999/Level2a_IC79_corsika.008316.000000.i3.bz2
# Directory where output files are saved
outdir=/data/user/jgarber/reco/8316

for data in $datafiles
do
    ofile=$outdir/$(basename $data)
    python /home/jgarber/IC79/process/process.py $gcd $data $ofile -s
done
