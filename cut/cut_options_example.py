"""
This file contains the cut options. The general format for the cut dictionaries
is

dict['FrameKeyString'] = boolean lambda function

For example,

event_cuts['ndir'] = lambda x: x > 5

means only keep frames with an ndir greater than 5.
"""

import numpy as np

IC_strings = [26, 27, 37, 46, 45, 35, 17, 18, 19, 28, 38, 47, 56, 55, 54, 44, 34, 25]
DC_strings = [81, 82, 83, 84, 85, 86]

# The event cuts to make. Change these as much as you like.
# Type signature of lambda functions is float -> bool
event_cuts = {}
event_cuts['ndir'] = lambda x: x > 5
event_cuts['nchan'] = lambda x: x > 20
event_cuts['rlogl'] = lambda x: x < 10
event_cuts['ICNHits'] = lambda x: x < 20
event_cuts['RecoEndpointZ'] = lambda x: x > -400
event_cuts['DistToBorder'] = lambda x: x > 50
event_cuts['ICAnalysisHits'] = lambda x: x > 0

# The dom cuts to make. Change these freely.
# Type signature of lambda functions is numpy.ndarray[float] -> numpy.ndarray[bool]
dom_cuts = {}
dom_cuts['String'] = lambda x: np.in1d(x, IC_strings)
dom_cuts['ImpactAngle'] = lambda x: x < np.pi / 2       # Must be radians
dom_cuts['DistAboveEndpoint'] = lambda x: x > 100

# The keys containing the per DOM data
dom_keys = ['TotalCharge', 'String', 'OM', 'DistAboveEndpoint', 'ImpactAngle', 'RecoDistance']
